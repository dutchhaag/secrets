require 'tmpdir'
require 'English'

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def image_name
    ENV.fetch('TMP_IMAGE', 'secrets:latest')
  end

  def privileged
    ENV.fetch('PRIVILEGED', false)
  end

  def target_mount_dir
    '/app'
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context 'with test project' do
    def parse_expected_report(expectation_name, report_name = "gl-secret-detection-report.json")
      path = File.join(expectations_dir, expectation_name, report_name)
      if ENV['REFRESH_EXPECTED'] == "true"
        # overwrite the expected JSON with the newly generated JSON
        FileUtils.cp(scan.report_path, File.expand_path(path))
      end
      JSON.parse(File.read(path))
    end

    let(:global_vars) do
      {
        'ANALYZER_INDENT_REPORT': 'true'
      }
    end

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables),
        report_filename: 'gl-secret-detection-report.json',
        privileged: privileged)
    end

    let(:report) { scan.report }

    context 'by default' do
      let(:project) { 'secrets' }

      it_behaves_like 'successful scan'

      describe 'created report' do
        it_behaves_like 'non-empty report'
        it_behaves_like 'recorded report' do
          let(:recorded_report) {
            parse_expected_report(project)
          }
        end
        it_behaves_like 'valid report'
      end
    end

    context 'with custom rulesets' do
      context 'with file passthrough' do
        let(:project) { 'custom_ruleset_config' }
        let(:variables) do
          {
            'GITLAB_FEATURES': 'sast_custom_rulesets'
          }
        end

        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report('secrets-custom-ruleset-file')
            }
          end
          it_behaves_like 'valid report'
        end
      end

      context 'with raw passthrough' do
        let(:project) { 'custom_ruleset_raw_config' }
        let(:variables) do
          {
            'GITLAB_FEATURES': 'sast_custom_rulesets'
          }
        end

        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report('secrets-custom-ruleset-file')
            }
          end
          it_behaves_like 'valid report'
        end
      end
    end
  end
end