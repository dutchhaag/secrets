package main

import (
	"flag"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command/v2"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

func mockCLI(flags map[string]string) *cli.Context {
	app := cli.NewApp()
	app.Commands = command.NewCommands(command.Config{
		Match:        func(path string, info os.FileInfo) (bool, error) { return true, nil },
		Convert:      func(r io.Reader, p string) (*report.Report, error) { return &report.Report{}, nil },
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
	})
	set := flag.NewFlagSet("testFlagSet", 0)
	for k, v := range flags {
		set.String(k, v, "")
	}
	return cli.NewContext(app, set, nil)
}
func TestConfigPath(t *testing.T) {
	rootPath := "/root/path"
	tests := []struct {
		name string
		in   *ruleset.Config
		want string
	}{
		{
			name: "nil ruleset",
			in:   nil,
			want: defaultPathGitleaksConfig,
		},
		{
			name: "passthrough without a matching target",
			in: &ruleset.Config{
				Passthrough: []ruleset.Passthrough{
					{
						Type:  ruleset.PassthroughFile,
						Value: "gitleaks-config.toml",
					},
				},
			},
			want: defaultPathGitleaksConfig,
		},
		{
			name: "Passthrough File",
			in: &ruleset.Config{
				Passthrough: []ruleset.Passthrough{
					{
						Type:   ruleset.PassthroughFile,
						Target: "gitleaks.toml",
						Value:  "gitleaks-config.toml",
					},
				},
			},
			want: filepath.Join(rootPath, "gitleaks-config.toml"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			path, err := configPath(rootPath, tt.in)

			if err != nil {
				t.Errorf("Expected a nil err, but got: %v", err)
			}

			assert.Equal(t, path, tt.want)
		})
	}
}

func TestConfigPathWithPassthroughRaw(t *testing.T) {
	rawGitleaksConfig := "the most raw"
	rs := &ruleset.Config{
		Passthrough: []ruleset.Passthrough{
			{
				Type:   ruleset.PassthroughRaw,
				Target: "gitleaks.toml",
				Value:  rawGitleaksConfig,
			},
		},
	}
	path, err := configPath("/root/path", rs)

	if err != nil {
		t.Errorf("Expected a nil err, but got: %v", err)
	}

	content, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatalf("tried to read %s, but got %v", path, err)
	}

	assert.Equal(t, string(content), rawGitleaksConfig)
}
